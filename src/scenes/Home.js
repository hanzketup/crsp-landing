import React from 'react'
import { Helmet } from 'react-helmet'
import {
  Box,
  Container,
  Image,
  Text,
  Flex,
  Heading,
  Button
} from '@chakra-ui/react'

import chips from 'style/chips.png'

const Home = () => {

  return (
    <Box>
      <Helmet>
        <title>Home | CRSP co.</title>
      </Helmet>

      <Container my="20vh">
        <Flex flexDir="column" align="center">
        <Image w="120px" src={chips} />
        <Heading mt="10" size="2xl">CR<Text as="sup">i</Text>SP<Text as="sub">y</Text></Heading>
        <Button mt="4" variant="ghost" size="sm" as="a" href="mailto: hannes@crsp.co">Lets talk</Button>
        </Flex>
      </Container>
    </Box>
  )
}

export default Home
