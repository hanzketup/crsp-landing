import React from 'react'
import { Box, Flex, Heading, Text } from '@chakra-ui/react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Option = ({ icon, title, description, leftSide, onClick }) => {
  return (
    <Box
      onClick={onClick}
      rounded="md"
      boxShadow="md"
      border="2px"
      borderColor="gray.200"
      py="5"
      px="8"
      my="2"
      mb="4"
      cursor="pointer"
      transition="0.3s"
      _hover={{ boxShadow: 'lg', transform: 'scale(1.01)' }}>
      <Flex>
        <Box w="65px" color="gray.600" mr="2">
          {leftSide ? (
            leftSide
          ) : (
            <FontAwesomeIcon size="4x" icon={['fal', icon]} />
          )}
        </Box>
        <Box px="4">
          <Heading color="gray.700" fontSize="xl">
            {title}
          </Heading>
          <Text color="gray.600" fontSize="sm">
            {description}
          </Text>
        </Box>
      </Flex>
    </Box>
  )
}

export default Option
